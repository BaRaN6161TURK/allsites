<?php

/**
 * Curse Inc.
 * All Sites
 * Change company name in footer
 *
 * @copyright	(c) 2019 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		All Sites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites\Maintenance;

use DynamicSettings\Wiki;
use Maintenance;

require_once dirname(__DIR__, 3) . "/maintenance/Maintenance.php";

class updateSiteOwner extends Maintenance {
	/**
	 * Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct();
		$this->mDescription = "Job to update owner in footer every time we are acquired";
	}

	/**
	 *
	 *
	 * @access	public
	 * @return	void
	 */
	public function execute() {
		$sites = Wiki::loadAll();
		$oldOwner = "Wikia, Inc.";

		foreach ($sites as $siteKey => $wiki) {
			$modifiedValue = false;
			$wikiDomain = $wiki->getDomain();
			$wikiDomain = str_replace([".wiki",".io"],".com",$wikiDomain);
			echo "Processing $wikiDomain\n";

			$settings = $wiki->getSettings();
			foreach ($settings as $i => $v) {
				if ($v->getSettingKey() == '$wgHydraSkinDisclaimer') {
					$disclaimerSetting = $v->getValue();
					if( strpos( $disclaimerSetting, $oldOwner ) !== false) {
						$disclaimerSetting = str_replace($oldOwner, "Fandom, Inc.", $disclaimerSetting);
						$settings[$i]->setValueFromForm($disclaimerSetting);
						$modifiedValue = true;
					}
				}
			}

			if($modifiedValue == true) {
				$wiki->saveSettings($settings, "Update owner name in the footer");
				echo "Updating footer with new owner name.\n";
			}
		}
	}
}

$maintClass = 'AllSites\\Maintenance\\updateSiteOwner';
require_once RUN_MAINTENANCE_IF_MAIN;
