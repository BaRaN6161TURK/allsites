<?php

/**
 * Curse Inc.
 * AllSites
 * UserCount Class
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2015 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AllSites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites;

use RedisException;
use Wikimedia\Rdbms\DatabaseMysqli;

class UserCount {
	/**
	 * Database Access
	 *
	 * @var		object
	 */
	private $DB;

	/**
	 * Redis Storage
	 *
	 * @var		object
	 */
	private $redis = false;

	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @param	object $DB MediaWiki Database
	 * @param	object $redis Redis Connection
	 * @return	void
	 */
	public function __construct(DatabaseMysqli $DB, $redis) {
		$this->DB = $DB;

		$this->redis = $redis;
	}

	/**
	 * Push unique users into Redis.
	 *
	 * @access	public
	 * @return	void
	 */
	public function pushUniqueUsers() {
		$result = $this->DB->select(
			['user'],
			['COUNT(*) as total'],
			null,
			__METHOD__
		);
		$total = $result->fetchRow();

		for ($i = 0; $i <= $total['total']; $i = $i + 1000) {
			//@TODO: This code is broken if HydraAuth is disabled.
			$results = $this->DB->select(
				['user', 'user_global'],
				[
					'user_name',
					'user_editcount',
					'global_id'
				],
				null,
				__METHOD__,
				[
					'OFFSET'	=> $i,
					'LIMIT'		=> 1000
				],
				[
					'user_global' => [
						'LEFT JOIN', 'user_global.user_id = user.user_id'
					]
				]
			);

			while ($row = $results->fetchRow()) {
				if (!empty($row['user_name'])) {
					$this->addUniqueName($row['user_name']);
				}

				if (!empty($row['user_name']) && $row['user_editcount'] > 0) {
					$this->addUniqueNameWithEdits($row['user_name']);
				}

				if ($row['global_id'] > 0) {
					$this->addUniqueGlobalId($row['global_id']);
				}

				if ($row['global_id'] > 0 && $row['user_editcount'] > 0) {
					$this->addUniqueGlobalIdWithEdits($row['global_id']);
				}
			}
		}
	}

	/**
	 * Add a name into the unique names set.
	 *
	 * @access	public
	 * @param	string	$name User Name
	 * @return	void
	 */
	public function addUniqueName($name) {
		try {
			if ($this->redis !== false) {
				$this->redis->sAdd('userCount:uniqueNames', $name);
			}
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Add a name into the unique names with edits set.
	 *
	 * @access	public
	 * @param	string	$name User Name
	 * @return	void
	 */
	public function addUniqueNameWithEdits($name) {
		try {
			if ($this->redis !== false) {
				$this->redis->sAdd('userCount:uniqueNamesWithEdits', $name);
			}
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Add a Global Id into the unique Global Ids set.
	 *
	 * @access	public
	 * @param	int	$globalId Curse Id
	 * @return	void
	 */
	public function addUniqueGlobalId($globalId) {
		try {
			if ($this->redis !== false) {
				$this->redis->sAdd('userCount:uniqueCurseIds', intval($globalId));
			}
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Add a Global Id into the unique Global IDs with edits set.
	 *
	 * @access	public
	 * @param	int $globalId Global ID
	 * @return	void
	 */
	public function addUniqueGlobalIdWithEdits($globalId) {
		try {
			if ($this->redis !== false) {
				$this->redis->sAdd('userCount:uniqueCurseIdsWithEdits', intval($globalId));
			}
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Return number of unique names.
	 *
	 * @access	public
	 * @return	integer	Number of Unique Names
	 */
	public function getUniqueNamesCount() {
		if ($this->redis === false) {
			return 0;
		}
		try {
			return intval($this->redis->sCard('userCount:uniqueNames'));
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Return number of unique names with edits.
	 *
	 * @access	public
	 * @return	integer	Number of Unique Names that have edits
	 */
	public function getUniqueNamesWithEditsCount() {
		if ($this->redis === false) {
			return 0;
		}
		try {
			return intval($this->redis->sCard('userCount:uniqueNamesWithEdits'));
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Return number of unique Curse Ids.
	 *
	 * @access	public
	 * @return	integer	Number of Unique Curse Ids
	 */
	public function getUniqueGlobalIdsCount() {
		if ($this->redis === false) {
			return 0;
		}
		try {
			return intval($this->redis->sCard('userCount:uniqueCurseIds'));
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}

	/**
	 * Return number of unique Curse Ids with edits.
	 *
	 * @access	public
	 * @return	integer	Number of Unique Curse Ids with edits.
	 */
	public function getUniqueGlobalIdsWithEditsCount() {
		if ($this->redis === false) {
			return 0;
		}
		try {
			return intval($this->redis->sCard('userCount:uniqueCurseIdsWithEdits'));
		} catch (RedisException $e) {
			wfDebug(__METHOD__ . ": Caught RedisException - " . $e->getMessage());
		}
	}
}
