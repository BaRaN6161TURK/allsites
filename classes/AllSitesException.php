<?php
/**
 * AllSites
 * AllSites API Exception
 *
 * @author		Samuel Hilson
 * @copyright	(c) 2018 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		AllSites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites;

use Exception;
use MWException;

class AllSitesException extends MWException {

	/**
	 * Constructor for Exception
	 *
	 * @access	public
	 * @param	string $message Exception message
	 * @param	int $code Status code
	 * @param	Exception|null $previous Previous exception
	 */
	public function __construct($message, $code = 0, Exception $previous = null) {
		parent::__construct($message, $code, $previous);
	}

	/**
	 * Return a string of the exception message and code
	 *
	 * @return string
	 */
	public function __toString() {
		return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
	}
}
