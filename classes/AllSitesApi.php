<?php

/**
 * DynamicSettings
 * DynamicSettings API
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2014 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		DynamicSettings
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites;

use ApiBase;

class AllSitesApi extends ApiBase {
	/**
	 * API Initialized
	 *
	 * @var		boolean
	 */
	private $initialized = false;

	/**
	 * Valid fields to pull.
	 *
	 * @var		array
	 */
	private $validFields = [
		'name'			=> 'getName',
		'meta_name'		=> 'getMetaName',
		'category'		=> 'getCategory',
		'tags'			=> 'getTags',
		'language'		=> 'getLanguage'
	];

	/**
	 * Initiates some needed classes.
	 *
	 * @access	public
	 * @return	void
	 */
	private function init() {
		if (!$this->initialized) {
			global $wgUser, $wgRequest;
			$this->wgUser		= $wgUser;
			$this->wgRequest	= $wgRequest;

			$this->initialized = true;
		}
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @return	void	[Outputs to screen]
	 */
	public function execute() {
		$this->init();

		$this->params = $this->extractRequestParams();

		switch ($this->params['do']) {
			// allow for an empty or missing do param
			// but still get angry if do does not match a specific action
			case '':
			case 'getSiteStats':
				$response = $this->getSiteStats();
				break;
			default:
				$this->dieWithError(['invaliddo', $this->params['do']]);
				break;
		}

		foreach ($response as $key => $value) {
			$this->getResult()->addValue(null, $key, $value);
		}
	}

	/**
	 * Requirements for API call parameters.
	 *
	 * @access	public
	 * @return	array	Merged array of parameter requirements.
	 */
	public function getAllowedParams() {
		return [
			'do' => [
				ApiBase::PARAM_TYPE		=>	'string',
				ApiBase::PARAM_REQUIRED	=>	false
			],
			'lastModified' => [
				ApiBase::PARAM_TYPE		=>	'integer',
				ApiBase::PARAM_REQUIRED	=>	false
			],
			'siteKey' => [
				ApiBase::PARAM_TYPE		=>	'string',
				ApiBase::PARAM_REQUIRED	=>	false,
			],
			'site_key' => [
				ApiBase::PARAM_TYPE		=>	'string',
				ApiBase::PARAM_REQUIRED	=>	false,
				ApiBase::PARAM_DEPRECATED => true
			],
			'filter' => [
				ApiBase::PARAM_TYPE		=>	'string',
				ApiBase::PARAM_REQUIRED	=>	false,
				ApiBase::PARAM_ISMULTI => true
			]
		];
	}

	/**
	 * Get information on an user by Curse ID.
	 *
	 * @access	public
	 * @return	bool Success
	 */
	public function getSiteStats() {
		$siteInformation = new SiteInformation();

		try {
			$data = $siteInformation->getSiteStats(
				$this->params['lastModified'],
				$this->params['siteKey'],
				$this->params['filter'],
				$this->params['site_key']
			);
		} catch (AllSitesException $ase) {
			return [
				'status' => 'error',
				'message' => $ase->getMessage()
			];
		}

		return ['status' => 'okay', 'data' => $data];
	}

	/**
	 * Get version of this API Extension.
	 *
	 * @access	public
	 * @return	string	API Extension Version
	 */
	public function getVersion() {
		return '1.0';
	}

	/**
	 * Return a ApiFormatJson format object.
	 *
	 * @access	public
	 * @return	object	ApiFormatJson
	 */
	public function getCustomPrinter() {
		return $this->getMain()->createPrinterByName('json');
	}

	/**
	 * @see ApiBase::getExamplesMessages()
	 * @return array
	 */
	protected function getExamplesMessages() {
		return [
			'action=allsites&format=json&formatversion=2'
				=> 'apihelp-allsites-example-all',
			'action=allsites&format=json&formatversion=2&lastModified=1534881509'
				=> 'apihelp-allsites-example-lastmodified',
			'action=allsites&format=json&formatversion=2&siteKey=814eff22aaa6e6bd72c91dfd23730bcf'
				=> 'apihelp-allsites-example-sitekey',
			'action=allsites&format=json&formatversion=2&filter=totals,wikis|ss_total_wikis,wiki_name,wiki_domain,wiki_category,wiki_tags'
				=> 'apihelp-allsites-example-filter'
		];
	}
}
