<?php

/**
 * Curse Inc.
 * All Sites
 * Build Site Stats SyncService Cron
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2014 Curse Inc.
 * @license		Proprietary
 * @package		All Sites
 * @link		https://gitlab.com/hydrawiki
 *
 **/

namespace AllSites\Jobs;

use SyncService\Job;
use AllSites\SiteInformation;

class BuildSiteStats extends Job {
	/**
	 * Builds site statistics for the entire wiki farm.
	 *
	 * @access	public
	 * @param	array	$args [Unused]
	 * @return	integer	Exit value for this thread.
	 */
	public function execute($args = []) {
		$siteInformation = new SiteInformation();
		$siteInformation->buildAllSiteStats();

		return 0;
	}

	/**
	 * Return cron schedule if applicable.
	 *
	 * @access	public
	 * @return	mixed	False for no schedule or an array of schedule information.
	 */
	public static function getSchedule() {
		return [
			[
				'minutes' => 0,
				'hours' => '*',
				'days' => '*',
				'months' => '*',
				'weekdays' => '*',
				'arguments' => []
			]
		];
	}
}
